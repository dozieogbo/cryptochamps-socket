const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const index = require('./routes/index');

const app = express();

const server = require('http').createServer(app);

const io = require('socket.io')(server, {
    origins: 'http://cryptochamps.co:* http://cryptochamps.test:8000'
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.send({
        status: err.status,
        message: err.message
    });
});

io.on('connection', socket => {
    socket.on('join', data => {
        socket.join('live-feed');
    });

    socket.on('purchase', data => {
        socket.broadcast.to('live-feed').emit('purchase', data);
    });
});

app.server = server;

module.exports = app;